<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="../assets/try/materialize/css/materialize.min.css"  media="screen,projection"/>

  <!-- mine -->
  <link rel="stylesheet" href="../assets/try/css./css.css">

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
  <!-- navbar -->
  <div class="navbar-fixed">
    <nav class="orange darken-3">
      <div class="container">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo">EasBlog</a>
          <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="#">Donate</a></li>
            <li><a href="#" class="modal-trigger" data-target="modal1">Sign In</a></li>
            <li><a href="#" class="modal-trigger" data-target="modal2">Sign Up</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>

  <ul class="sidenav" id="mobile-nav">
    <li><a href="#">Donate</a></li>
    <li><a href="#" class="modal-trigger" data-target="modal1">Sign In</a></li>
    <li><a href="#" class="modal-trigger" data-target="modal2">Sign Up</a></li>
  </ul>
  <!-- end navbar -->

  <!-- slideshow -->
  <div class="slider">
    <ul class="slides">
      <li>
        <img src="../assets/try/img/slideshow/1.jpg">
        <div class="caption right-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
          <button class="btn waves-effect waves-light pulse" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
      </li>
      <li>
        <img src="../assets/try/img/slideshow/2.jpg">
        <div class="caption center-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
          <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
      </li>
      <li>
        <img src="../assets/try/img/slideshow/4.jpg">
        <div class="caption left-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
          <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
      </li>
    </ul>
  </div>
  <!-- end slideshow -->

  <!-- about -->
  <caption id="about" class="about">
    <div class="container">
      <div class="row">
        <h3 class="center light grey-text text-darken-3">About Us</h3>
        <div class="col s6 light">
          <h5>This Things</h5>
          <p class="valign-wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam nulla assumenda similique eos molestiae, illum vero! Illo sunt nostrum quo, laboriosam. Asperiores commodi corporis aspernatur libero in animi dicta delectus!</p>
        </div>
        <div class="col s6">
          <p>Easy Way To Make Blog</p>
          <div class="progress">
            <div class="determinate" style="width: 85%"></div>
          </div>
          <p>No Need To Spend Money</p>
          <div class="progress">
            <div class="determinate" style="width: 100%"></div>
          </div>
          <p>Fastest Way To Make Money</p>
          <div class="progress">
            <div class="determinate" style="width: 90%"></div>
          </div>
        </div>
      </div>
    </div>
  </caption>
  <!-- end about -->

  <!-- parallax -->
  <div class="parallax-container">
    <div class="parallax"><img src="../assets/try/img/slideshow/3.jpg"></div>

    <div class="container sponsor">
      <h3 class="center light white-text">Sponsored By</h3>

      <div class="row">
        <div class="col m6 s12 center">
          <img src="img/Sponsor/blog.png" alt="">
        </div>
        <div class="col m6 s12 center">
          <img src="img/Sponsor/blog.png" alt="">
        </div>
      </div>
    </div>
  </div>
  <!-- end parallax -->

  <!-- modal -->
  <!-- sign in -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Sign In</h4>
      <div class="row">
        <div class="row">
          <div class="input-field col s12">
            <input id="first_name" type="text" class="validate">
            <label for="first_name">Username</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" class="validate">
            <label for="password">Password</label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col m3 s6">
          <a class="waves-effect waves-light btn orange lighten-1"><i class="material-icons left">cloud</i>Google</a>
        </div>
        <div class="col m3 s6">
          <a class="waves-effect waves-light btn orange lighten-1"><i class="material-icons left">cloud</i>Facebook</a>
        </div>
      </div>
    </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn orange">Sign In</a>
      </div>
  </div>
  <!-- end sign in -->
  <!-- register -->
  <div id="modal2" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Sign Up</h4>
      <div class="row">
        <div class="row">
          <div class="input-field col s12">
            <input id="first_name" type="text" class="validate">
            <label for="first_name">Username</label>
          </div>
        </div>
         <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" class="validate">
            <label for="password">Email</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" class="validate">
            <label for="password">Password</label>
          </div>
        </div>
      </div>
     </div>
    <div class="modal-footer">
      <a class="waves-effect waves-light btn orange">Sign Up</a>
    </div>
  </div>
  <!-- end register -->
  <!-- end modal -->

  <!-- card -->
  <section id="service" class="service grey lighten-3">
    <div class="container">
      <div class="row">
        <h3 class="light center grey-text text-darken-3">Our Services</h3>
          <div class="col m4 s12">
            <div class="card-panel center">
              <i class="material-icons medium">domain</i>
              <h4>Easy Way To Make Blog</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus dolorem repellat laboriosam veniam id cumque nam nobis alias eaque recusandae tempore, dolore animi cum, pariatur. Molestiae unde dolore officia nulla.</p>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card-panel center">
              <i class="material-icons medium">input</i>
              <h4>No Need To Spend Money</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus dolorem repellat laboriosam veniam id cumque nam nobis alias eaque recusandae tempore, dolore animi cum, pariatur. Molestiae unde dolore officia nulla.</p>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card-panel center">
              <i class="material-icons medium">attach_money</i>
              <h4>Fastest To Make Money</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus dolorem repellat laboriosam veniam id cumque nam nobis alias eaque recusandae tempore, dolore animi cum, pariatur. Molestiae unde dolore officia nulla.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end card -->

  <!-- footer -->
  <section id="footer" class="footer">
    <div class="page-footer orange darken-3">
            <div class="container center">
            © 2019 Copyright Text
            </div>
          </div>
  </section>
  <!-- footer -->

  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
  <script>

    const sidenav = document.querySelectorAll('.sidenav');
    M.Sidenav.init(sidenav);

    const slider = document.querySelectorAll('.slider');
    M.Slider.init(slider, {
      indicators: false,
      interval: 3000
    });

    const parallax = document.querySelectorAll('.parallax');
    M.Parallax.init(parallax);

    const modal = document.querySelectorAll('.modal');
    M.Modal.init(modal);

  </script>
</body>
</html>