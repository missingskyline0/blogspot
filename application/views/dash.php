<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets/css/admin.css">

  <title>Dashboard Admin</title>
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
    <a class="navbar-brand ml-3 display-3" href="#">GSPOT</a>
    <form class="form-inline my-2 my-lg-0 ml-auto">
    </form>
    <div class="icon ml-4">
      <h5>
        <a href="profil_akun.html"><i class="fa fa-user-circle-o mr-3 usrrr" data-toggle="tooltip" title="Akun"></i></a> 
        <i class="fa fa-bell mr-3"data-toggle="tooltip" title="Notifikasi"></i>
        <a href="index.html"><i class="fa fa-sign-out mr-3"data-toggle="tooltip" title="Sing Out"></i></a>

      </h5>

    </div>
  </div>
</nav>

<div class="row no-gutters mt-5">
  <div class="col-md-2 bg-dark mt-2 pr-3 pt-4">
    <ul class="nav flex-column ml-3 mb-5">
      <a href="tulis.html">
        <button class="btn btn-outline bg-white text-black mr-3 my-3 my-sm-2" type="submit"><i class="fa fa-plus"></i>Tulis</button></a>
        <li class="nav-item">
          <a class="nav-link active text-white" href="dashboard.html"><i class="fa fa-tachometer mr-2"></i>Dashboard</a><hr class="bg-secondary">
        </li>
        <li class="nav-item">
          <a class="nav-link  text-white" href="posting.html"><i class="fa fa-clipboard mr-2"></i>Postingan</a><hr class="bg-secondary">
        </li>
        <li class="nav-item">
          <a class="nav-link  text-white" href="#"><i class="fa fa-bar-chart mr-2" ></i>Statistik</a><hr class="bg-secondary">
        </li>
        <li class="nav-item">
          <a class="nav-link  text-white" href="#"><i class="fa fa-comments-o mr-2"></i>Komentar</a><hr class="bg-secondary">
        </li>
        <li class="nav-item">
          <a class="nav-link  text-white" href="bacaan.html"><i class="fa fa-bookmark mr-2"></i>Daftar Bacaan</a><hr class="bg-secondary">
        </li>
        <li class="nav-item">
          <a class="nav-link  text-white" href="#"><i class="fa fa-cog mr-2"></i>Bantuan</a><hr class="bg-secondary">
        </li>
      </ul>
    </div>
    <div class="col-md-10 p-5 pt-2">
      <h3><i class="fa fa-tachometer mr-2"></i>Dashboard</i></h3><hr>

      <div class="row text-white">
        <div class="card bg-info ml-5" style="width: 18rem;">
          <div class="card-body">
            <div class="card-body-icon"> 
              <i class="fa fa-users" aria-hidden="true"></i>
            </div>
            <h5 class="card-title ">Daftar Pengunjung</h5>
            <div class="display-4">1.760</div>
            <a href="#"> <p class="card-text text-white">Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></p></a>
          </div>
        </div>
        <div class="card bg-success ml-5" style="width: 18rem;">
          <div class="card-body">
            <div class="card-body-icon"> 
              <i class="fa fa-clipboard" aria-hidden="true"></i>
            </div>
            <h5 class="card-title ">Postingan</h5>
            <div class="display-4">1.000</div>
            <a href="#"> <p class="card-text text-white">Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></p></a>
          </div>
        </div>
        <div class="card bg-warning ml-5" style="width: 18rem;">
          <div class="card-body">
            <div class="card-body-icon"> 
              <i class="fa fa-bookmark" aria-hidden="true"></i>
            </div>
            <h5 class="card-title ">Daftar Bacaan </h5>
            <div class="display-4">1.760</div>
            <a href="#"> <p class="card-text text-white">Lihat Detail <i class="fas fa-angle-double-right ml-2"></i></p></a>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script type="tetx/javascript" src="../assets/js/admin.js"></script>
</body>
</html>

<!-- https://en.savefrom.net/#url=http://youtube.com/watch?v=LCsIQW7PoP8&utm_source=youtube.com&utm_medium=short_domains&utm_campaign=www.ssyoutube.com&a_ts=1568870821.592 -->